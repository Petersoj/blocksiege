package com.blocksiege.game;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.block.BlockState;
import org.bukkit.material.MaterialData;
import org.bukkit.util.Vector;

import com.blocksiege.util.HitBox;
import com.blocksiege.util.NMSUtils;
import com.blocksiege.world.GameWorld;
import com.blocksiege.world.MapTeam;

import net.minecraft.server.v1_11_R1.EntityArmorStand;

public class Flag {
	
	private GameWorld gameWorld;
	
	private Location flagLocation;
	private HitBox miniMapHitBox;
	private EntityArmorStand flagArmorStand;
	private MapTeam flagTeam;
	
	public Flag(GameWorld gameWorld, Location flagLocation, Location miniHitbox){
		this.gameWorld = gameWorld;
		this.flagLocation = flagLocation;
		
		this.miniMapHitBox = new HitBox(miniHitbox.toVector(), miniHitbox.toVector().add(new Vector(0.19, 0.19, 0.19)));
		
		this.createFlagArmorStand();
	}
	
	private void createFlagArmorStand(){
		this.flagArmorStand = NMSUtils.getNewArmorStand(gameWorld);
		
	}
	
	public void raise(){
		
	}
	
	public void lower(){
		
	}
	
	public void setFlagTeam(MapTeam mapTeam){
		this.flagTeam = mapTeam;
		changeFlagPost(mapTeam);
//		changeFlagWool(mapTeam);
	}
	
	private void changeFlagPost(MapTeam mapTeam){
		Location toChangeLoc = flagLocation.clone();
		for(int y = 0; y <= 8; y++){
			BlockState newState = toChangeLoc.getBlock().getState();
			if(mapTeam == null){
				newState.setType(MapTeam.DEFAULT_FENCE.getType());
				newState.setData(MapTeam.DEFAULT_FENCE.getData());
			}else{
				newState.setType(mapTeam.getFlagFence().getType());
				newState.setData(mapTeam.getFlagFence().getData());
			}
			newState.update(true);

			gameWorld.getWorld().playEffect(toChangeLoc, Effect.STEP_SOUND, newState.getTypeId());
			
			toChangeLoc.add(0, 1, 0);
		}
	}
	
	private void changeFlagWool(MapTeam mapTeam){
		Location currentLoc = flagLocation.clone();
		
		// Propably could have done this with a loop :|
		currentLoc.add(0, 0, 4);
		updateWool(currentLoc.getBlock().getState(), mapTeam);
		currentLoc.add(1, 0, 0);
		updateWool(currentLoc.getBlock().getState(), mapTeam);
		currentLoc.add(1, 0, -1);
		updateWool(currentLoc.getBlock().getState(), mapTeam);
		currentLoc.add(1, 0, -1);
		updateWool(currentLoc.getBlock().getState(), mapTeam);
		currentLoc.add(1, 0, -1);
		updateWool(currentLoc.getBlock().getState(), mapTeam);
		currentLoc.add(0, 0, -1);
		updateWool(currentLoc.getBlock().getState(), mapTeam);
		currentLoc.add(0, 0, -1);
		updateWool(currentLoc.getBlock().getState(), mapTeam);
		currentLoc.add(-1, 0, -1);
		updateWool(currentLoc.getBlock().getState(), mapTeam);
		currentLoc.add(-1, 0, -1);
		updateWool(currentLoc.getBlock().getState(), mapTeam);
		currentLoc.add(-1, 0, -1);
		updateWool(currentLoc.getBlock().getState(), mapTeam);
		currentLoc.add(-1, 0, 0);
		updateWool(currentLoc.getBlock().getState(), mapTeam);
		currentLoc.add(-1, 0, 0);
		updateWool(currentLoc.getBlock().getState(), mapTeam);
		currentLoc.add(-1, 0, 1);
		updateWool(currentLoc.getBlock().getState(), mapTeam);
		currentLoc.add(-1, 0, 1);
		updateWool(currentLoc.getBlock().getState(), mapTeam);
		currentLoc.add(-1, 0, 1);
		updateWool(currentLoc.getBlock().getState(), mapTeam);
		currentLoc.add(0, 0, 1);
		updateWool(currentLoc.getBlock().getState(), mapTeam);
		currentLoc.add(0, 0, 1);
		updateWool(currentLoc.getBlock().getState(), mapTeam);
		currentLoc.add(1, 0, 1);
		updateWool(currentLoc.getBlock().getState(), mapTeam);
		currentLoc.add(1, 0, 1);
		updateWool(currentLoc.getBlock().getState(), mapTeam);
		currentLoc.add(1, 0, 1);
		updateWool(currentLoc.getBlock().getState(), mapTeam);
	}
	private void updateWool(BlockState woolState, MapTeam mapTeam){
		if(mapTeam == null){
			System.out.println(woolState == null);
			System.out.println(MapTeam.DEFAULT_WOOL.getData() == null);
			woolState.setData(MapTeam.DEFAULT_WOOL.getData());
		}else{
			woolState.setData(mapTeam.getFlagWool().getData());
		}
		woolState.update(true);

		gameWorld.getWorld().playEffect(woolState.getLocation(), Effect.STEP_SOUND, woolState.getTypeId());
	}

	public GameWorld getGameWorld() {
		return gameWorld;
	}

	public Location getFlagLocation() {
		return flagLocation;
	}

	public HitBox getMiniMapHitBox() {
		return miniMapHitBox;
	}

	public MapTeam getFlagTeam() {
		return flagTeam;
	}
}
