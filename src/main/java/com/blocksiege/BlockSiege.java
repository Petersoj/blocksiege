package com.blocksiege;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.bukkit.Bukkit;
import org.bukkit.WorldCreator;
import org.bukkit.craftbukkit.v1_11_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import com.blocksiege.controller.DataController;
import com.blocksiege.controller.GameController;
import com.blocksiege.controller.PlayerController;
import com.blocksiege.controller.SpawnController;
import com.blocksiege.controller.TeamController;
import com.blocksiege.controller.WorldController;
import com.blocksiege.listener.Listeners;
import com.blocksiege.world.worlds.RohanWorld;

import net.minecraft.server.v1_11_R1.EntityArmorStand;
import net.minecraft.server.v1_11_R1.EnumItemSlot;
import net.minecraft.server.v1_11_R1.PacketPlayOutEntityEquipment;
import net.minecraft.server.v1_11_R1.PacketPlayOutSpawnEntityLiving;
import net.minecraft.server.v1_11_R1.PlayerConnection;

public class BlockSiege extends JavaPlugin implements Listener {

	private DataController dataController;
	private GameController gameController;
	private PlayerController playerController;
	private SpawnController spawnController;
	private TeamController teamController;
	private WorldController worldController;
	private Listeners listeners;
	
	@Override
	public void onEnable() {
		this.saveDefaultConfig();
		
		this.dataController = new DataController(this);
		this.gameController = new GameController(this);
//		this.listeners = new Listeners(this);
		
		this.gameController.start();

		Bukkit.getPluginManager().registerEvents(this, this);
	}

	@Override
	public void onDisable() {
		
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		new BukkitRunnable() {
			@Override
			public void run() {
				try{
					PreparedStatement statement = dataController.getHikariDataSource().getConnection().prepareStatement(dataController.getPlayerDataQuery());
					dataController.preparePlayerDataQuery(statement, e.getPlayer().getUniqueId());
					if(statement.execute()){
						ResultSet results = statement.getResultSet();
						while(results.next()){
							Bukkit.broadcastMessage(results.getString(2));
						}
					}
					statement.close();
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}.runTaskAsynchronously(this);
	}
	
	public DataController getDataController() {
		return dataController;
	}

	public GameController getGameController() {
		return gameController;
	}

	public PlayerController getPlayerController() {
		return playerController;
	}

	public SpawnController getSpawnController() {
		return spawnController;
	}

	public TeamController getTeamController() {
		return teamController;
	}

	public WorldController getWorldController() {
		return worldController;
	}

	public Listeners getListeners() {
		return listeners;
	}

	@EventHandler
	public void onCommand(PlayerCommandPreprocessEvent e) {
		Player player = e.getPlayer();
		String command = e.getMessage();

		if(command.toLowerCase().startsWith("/cw")){
			String worldName = command.split(" ")[1];
			Bukkit.createWorld(new WorldCreator(worldName));
		}
		
		if(command.toLowerCase().startsWith("/world")){
			String worldName = command.split(" ")[1];
			player.teleport(Bukkit.getWorld(worldName).getSpawnLocation());
		}
		
		if(command.equals("/createMini")){
			try {
				//Files.write(Paths.get(System.getProperty("user.home") + "/Desktop/map-data.txt"),
					//	Utils.generateMiniMapJSON(player, player.getLocation().getBlock().getLocation()).getBytes(), StandardOpenOption.CREATE_NEW);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	
		if(command.equals("/miniArmors")){
			long time = System.currentTimeMillis();
			RohanWorld world = new RohanWorld(this);
			System.out.println(System.currentTimeMillis() - time);
			for(EntityArmorStand armorStand : world.getGoodSpawn().getMiniMap().getMiniBlocks()){
				PlayerConnection playerConnection = ((CraftPlayer)player).getHandle().playerConnection;
				
				playerConnection.sendPacket(new PacketPlayOutSpawnEntityLiving(armorStand));
				
				PacketPlayOutEntityEquipment eq = new PacketPlayOutEntityEquipment(armorStand.getId(), EnumItemSlot.MAINHAND, armorStand.getEquipment(EnumItemSlot.MAINHAND));
				playerConnection.sendPacket(eq);
			}
			for(EntityArmorStand armorStand : world.getGoodSpawn().getMiniMap().getMiniFlags()){
				PlayerConnection playerConnection = ((CraftPlayer)player).getHandle().playerConnection;
				
				playerConnection.sendPacket(new PacketPlayOutSpawnEntityLiving(armorStand));
				
				PacketPlayOutEntityEquipment eq = new PacketPlayOutEntityEquipment(armorStand.getId(), EnumItemSlot.MAINHAND, armorStand.getEquipment(EnumItemSlot.MAINHAND));
				playerConnection.sendPacket(eq);
			}
			
//			for(Flag flag : world.getFlags()){
//				flag.setFlagTeam(null);
//			}
//			for(EntityArmorStand armorStand : world.getEvilSpawn().getMiniMap().getMiniBlocks()){
//				PlayerConnection playerConnection = ((CraftPlayer)player).getHandle().playerConnection;
//				
//				playerConnection.sendPacket(new PacketPlayOutSpawnEntityLiving(armorStand));
//				
//				PacketPlayOutEntityEquipment eq = new PacketPlayOutEntityEquipment(armorStand.getId(), EnumItemSlot.MAINHAND, armorStand.getEquipment(EnumItemSlot.MAINHAND));
//				playerConnection.sendPacket(eq);
//			}
		}
		
//		if(command.equals("/lightme")){
//			WorldServer worldServer = ((CraftWorld) player.getWorld()).getHandle();
//			BlockPosition pos1 = new BlockPosition(player.getLocation().getBlockX(), player.getLocation().getBlockY(), player.getLocation().getBlockZ());
//			worldServer.a(EnumSkyBlock.BLOCK, pos1, 15);
//			for(int x = -7; x < 8; x++){
//				for(int y = -7; y < 8; y++){
//					for(int z = -7; z < 8; z++){
//						Location loc = player.getLocation().add(x, y, z);
//
//						if(loc.getBlockX() == player.getLocation().getBlockX() && loc.getBlockY()== player.getLocation().getBlockY()&&loc.getBlockZ() == player.getLocation().getBlockZ()
//								&& loc.getBlock().getLightLevel() == 15){
//							System.out.println("juan");
//						}else{
//							BlockPosition position = new BlockPosition(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
////							worldServer.c(EnumSkyBlock.BLOCK, position); // recalculates the lighting of that block
//						}
//					}
//				}
//			}
//			
//			
//			Chunk c = ((CraftPlayer)player).getHandle().world.getChunkAtWorldCoords(((CraftPlayer)player).getHandle().getChunkCoordinates());
//			c.initLighting();
//			PacketPlayOutMapChunk pc = new PacketPlayOutMapChunk(c, 65534);
//			((CraftPlayer)player).getHandle().playerConnection.sendPacket(pc);
//		}
		
//		if(command.equals("/removeme")){
//			WorldServer worldServer = ((CraftWorld) player.getWorld()).getHandle();
//			for(int x = -17; x < 18; x++){
//				for(int y = -17; y < 18; y++){
//					for(int z = -17; z < 18; z++){
//						Location loc = player.getLocation().add(x, y, z);
//							BlockPosition position = new BlockPosition(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
//							worldServer.c(EnumSkyBlock.BLOCK, position);
//					}
//				}
//			}
//			
//			
//			Chunk c = ((CraftPlayer)player).getHandle().world.getChunkAtWorldCoords(((CraftPlayer)player).getHandle().getChunkCoordinates());
//			PacketPlayOutMapChunk pc = new PacketPlayOutMapChunk(c, 65534);
//			((CraftPlayer)player).getHandle().playerConnection.sendPacket(pc);
//		}
		
		if(command.equals("/ssss")){
//			EntityArmorStand armor =  NMSUtils.getNewArmorStand(new GameWorld(this, player.getWorld(), "data_helms_deep.json"));
//			armor.setInvisible(false);
//			NMSUtils.getNMSWorld(new GameWorld(this, player.getWorld(), "data_helms_deep.json")).addEntity(armor);
		}
		
		if(command.equals("/explode")){
//			Explosion explosion = new Explosion(null, e.getPlayer().getLocation(), new MapTeam(new GameWorld(this, Bukkit.getWorld("world"), "")), 3.5f);
//			explosion.explode();
		}
		
//		if(command.equals("/armor")){
//			ArmorStand armor = (ArmorStand) player.getWorld().spawnEntity(player.getLocation(), EntityType.ARMOR_STAND);
//			armor.setVelocity(player.getLocation().getDirection().multiply(2));
//			
//			System.out.println(armor.getVelocity());
//			
//			new BukkitRunnable() {
//				int count = 0;
//				public void run() {
//					if(count++ > 100){
//						this.cancel();
//					}
//					Vector vel = armor.getVelocity().normalize().multiply(1.3);
//					Block block = armor.getLocation().add(vel).getBlock();
//					if(block.getType() != Material.AIR){
//						Bukkit.broadcastMessage(block.getType().name());
//					}
//				}
//			}.runTaskTimer(this, 0, 1);
//		}
	}
}
