package com.blocksiege.kit;

public enum KitPossession {
	
	BOUGHT, UNLOCKED, LOCKED

}
