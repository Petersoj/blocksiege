package com.blocksiege.util;

import java.util.concurrent.ThreadLocalRandom;

public class MathUtils {

	private static double[] sin = new double[360];
	private static double[] cos = new double[360];

	static {
		for (int i = 0; i < 360; i++) {
			sin[i] = Math.sin(Math.toRadians(i));
			cos[i] = Math.cos(Math.toRadians(i));
		}
	}

	public static double getSine(int angle) {
		int angleCircle = angle % 360;
		if(angle < 0){
			return -sin[angleCircle * -1];
		}else{
			return sin[angleCircle];
		}
	}

	public static double getCosine(int angle) {
		int angleCircle = angle % 360;
		if(angle < 0){
			return -cos[angleCircle * -1];
		}else{
			return cos[angleCircle];
		}
	}
	
	public static int getRandomInt(int min, int max){
		return ThreadLocalRandom.current().nextInt(min, max + 1); // max + 1 for inclusive behavior
	}
	
	public static double getRandomDouble(int min, int max){
		return ThreadLocalRandom.current().nextDouble(min, max);
	}
	
	public static double getRandomDouble(double min, double max){
		return ThreadLocalRandom.current().nextDouble(min, max);
	}
}
