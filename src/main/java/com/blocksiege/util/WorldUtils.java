package com.blocksiege.util;

import java.util.ArrayList;

import org.bukkit.Location;
import org.bukkit.block.BlockState;

public class WorldUtils {
	
	public static ArrayList<Location> getSphereLocations(Location location, float radius, boolean hollow, boolean unevenEdge, float edgeRandomness) {

		ArrayList<Location> sphereBlocks = new ArrayList<Location>();

		float locX = (float) location.getX();
        float locY = (float) location.getY();
        float locZ = (float) location.getZ();
		
        // Loop through Y values last so it builds the list from the bottom up
		for(float y = locY - radius; y <= locY + radius; y++){
			for(float x = locX - radius; x <= locX + radius; x++){
				for(float z = locZ - radius; z <= locZ + radius; z++){
					
					double distance = ((locX - x) * (locX - x) + ((locZ - z) * (locZ - z)) + ((locY - y) * (locY - y)));
					
					boolean distanceCheck =  distance <= radius * radius; // check if distance^2 is less than radius^2
					boolean distanceMinusCheck = hollow || unevenEdge ? !(distance < (radius - 1) * (radius - 1)) : false;

					if(distanceCheck){
						if(distanceMinusCheck){
							if(hollow){
								sphereBlocks.add(new Location(location.getWorld(), x, y, z));
							}else if(unevenEdge){
								if(MathUtils.getRandomDouble(0, 1) > edgeRandomness){
									sphereBlocks.add(new Location(location.getWorld(), x, y, z));
								}
							}
						}else{
							sphereBlocks.add(new Location(location.getWorld(), x, y, z));
						}
					}
				}
			}
		}
		return sphereBlocks;
	}
	
	public static boolean compareBlockStates(BlockState blockState1, BlockState blockState2){
		if(blockState1.getType() == blockState2.getType()
				&& blockState1.getData().getData() == blockState2.getData().getData()
				&& blockState1.getRawData() == blockState2.getRawData()
				&& blockState1.getLocation().equals(blockState2.getLocation())){
			return true;
		}
		return false;
	}
}
