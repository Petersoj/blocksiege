package com.blocksiege.util;

import org.bukkit.ChatColor;

public class ChatUtils {
	
	public static String getFormedString(String title, String message){
		return ChatColor.GOLD + "<" + title + "> " + ChatColor.GRAY + message;
	}
}
