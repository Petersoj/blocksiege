package com.blocksiege.util;

import org.bukkit.Location;
import org.bukkit.util.Vector;

public class HitBox {
	
	private Vector min;
	private Vector max;
	
	public HitBox(Vector min, Vector max){
		this.min = min; // pass in negative values for min
		this.max = max;
	}
	
	public HitBox(int minX, int minY, int minZ, int maxX, int maxY, int maxZ){
		this(new Vector(minX, minY, minZ), new Vector(maxX, maxY, maxZ));
	}
	
	public boolean locationIsInside(Location location){
		return location.toVector().isInAABB(min, max);
	}
	
	public boolean locationIsInside(Vector locationAsVector){
		return locationAsVector.isInAABB(min, max);
	}

	public Vector getMin() {
		return min;
	}

	public void setMin(Vector min) {
		this.min = min;
	}

	public Vector getMax() {
		return max;
	}

	public void setMax(Vector max) {
		this.max = max;
	}
}
