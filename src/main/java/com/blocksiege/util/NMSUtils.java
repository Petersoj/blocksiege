package com.blocksiege.util;

import org.bukkit.craftbukkit.v1_11_R1.CraftWorld;

import com.blocksiege.world.GameWorld;

import net.minecraft.server.v1_11_R1.EntityArmorStand;
import net.minecraft.server.v1_11_R1.Vector3f;
import net.minecraft.server.v1_11_R1.World;

public class NMSUtils {
	
	public static World getNMSWorld(GameWorld gameWorld){
		return ((CraftWorld) gameWorld.getWorld()).getHandle();
	}
	
	public static EntityArmorStand getNewArmorStand(GameWorld gameWorld){
		EntityArmorStand armorStand = new EntityArmorStand(getNMSWorld(gameWorld));
		armorStand.setInvisible(true);
		armorStand.setBasePlate(false);
		armorStand.setInvulnerable(true);
		armorStand.setNoGravity(true);
		armorStand.setSilent(true);
		armorStand.setArms(true);
		armorStand.setRightArmPose(new Vector3f(345, 45, 0));
		
		return armorStand;
	}

}
