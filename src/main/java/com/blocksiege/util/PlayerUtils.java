package com.blocksiege.util;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import com.blocksiege.BlockSiege;

public class PlayerUtils {
	
	public static void deleteOverworldPlayerDataAsync(BlockSiege plugin){
		new BukkitRunnable() {
			public void run() {
				File pathToOverworld = new File(Bukkit.getWorldContainer().getPath() + File.separator + "world" + File.separator + "playerdata");
				for(File file : pathToOverworld.listFiles()){
					file.delete();
				}
			}
		}.runTaskAsynchronously(plugin);
	}
}