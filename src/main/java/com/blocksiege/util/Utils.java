package com.blocksiege.util;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_11_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_11_R1.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_11_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Step;
import org.bukkit.material.WoodenStep;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import net.minecraft.server.v1_11_R1.EntityArmorStand;
import net.minecraft.server.v1_11_R1.EnumItemSlot;
import net.minecraft.server.v1_11_R1.PacketPlayOutEntityEquipment;
import net.minecraft.server.v1_11_R1.PacketPlayOutSpawnEntityLiving;
import net.minecraft.server.v1_11_R1.PlayerConnection;
import net.minecraft.server.v1_11_R1.Vector3f;

public class Utils {
// A class full of arbitrary static methods
	
	// This method is not optimized and doesn't need to be.
	public static String generateMiniMapJSON(Player player, Location bottomLeftLocation){
		JSONObject mainObject = new JSONObject();
		
		JSONArray armorStandArray = new JSONArray();
		JSONArray flagArray = new JSONArray();
		
		World world = bottomLeftLocation.getWorld();
		
		// ArmorStand generation loops
		for(int x = 0; x < 13; x++){
			for(int y = 0; y < 13; y++){
				for(int z = 0; z < 13; z++){
					Block block = world.getBlockAt(bottomLeftLocation.clone().add(x, y, z));

					if(!(block != null && block.getType() != Material.AIR)){
						continue;
					}
					
					JSONObject armorStandObject = null;
					
					// Depricated because of "Magic Value", but there is no other way to do this.
					int materialID = block.getTypeId();
					int materialData = block.getData();
					
					Location deltaLocation = new Location(bottomLeftLocation.getWorld(), 0, 0, 0);
					// same as multiply, but .multiply adds a ton of 0's for some reason.
					deltaLocation.setX(x * 0.37501);
					deltaLocation.setY(y * 0.37501);
					deltaLocation.setZ(z * 0.37501);
					
					if(block.getType() == Material.WOOD_STEP || block.getType() == Material.STEP){
						boolean inverted = false;
						
						if(block.getType() == Material.STEP){
							Step step = ((Step) block.getState().getData());
							inverted = step.isInverted();
							if(inverted){
								step.setInverted(false);
								materialData = step.getData();
							}
						}else{ // WOOD_STEP
							WoodenStep woodStep = ((WoodenStep) block.getState().getData());
							inverted = woodStep.isInverted();
							if(inverted){
								woodStep.setInverted(false);
								materialData = woodStep.getData();
							}
						}
						
						if(inverted){
							deltaLocation.add(0.8505, -0.47, -0.075);
						}else{
							deltaLocation.add(0.8505, -0.6575, -0.075);
						}
						armorStandObject = getNewJSONArmorStand(deltaLocation, materialID, materialData);
						
					}else if(block.getType() == Material.CARPET){
						
						deltaLocation.add(0.8278, -0.779, -0.052);
						armorStandObject = getNewJSONArmorStand(deltaLocation, materialID, materialData);
						
					}else if(block.getType() == Material.WOOL){
						
						Block belowBlock = block.getLocation().clone().subtract(0, 0.5, 0).getBlock();
						boolean invertedSlabBelow = (belowBlock.getType() == Material.STEP ? ((Step) belowBlock.getState().getData()).isInverted()
								: (belowBlock.getType() == Material.WOOD_STEP ? ((WoodenStep) belowBlock.getState().getData()).isInverted() : true));
						
						
						Location miniHitBoxLocation = deltaLocation.clone();
						
						if(invertedSlabBelow){ // normal block
							deltaLocation.add(0.51875, -0.329, 0.05475);
						}else{ // put it to the top of the slab
							deltaLocation.add(0.51875, -0.5165, 0.05475);
						}
						
						flagArray.add(getNewFlagJSONArmorStand(new Location(null, 3,3,3), deltaLocation, miniHitBoxLocation));
						
					}else{ // regular block
						
						deltaLocation.add(0.8505, -0.6575, -0.075);
						armorStandObject = getNewJSONArmorStand(deltaLocation, materialID, materialData);
						
					}
					
					
					if(armorStandObject != null){
						armorStandArray.add(armorStandObject);
						createArmorStand(player, deltaLocation, materialID, materialData);
					}
				}
			}
		}

		// I will manually edit these values :|
		// http://www.jsonformatter.org

		JSONObject genericSpawnObject = new JSONObject();
		genericSpawnObject.put("x", 20);
		genericSpawnObject.put("y", 20);
		genericSpawnObject.put("z", 20);
		
		JSONObject genericSpawnYawObject = new JSONObject();
		genericSpawnObject.put("x", 20);
		genericSpawnObject.put("y", 20);
		genericSpawnObject.put("z", 20);
		genericSpawnObject.put("yaw", 0);
		
		JSONArray genericRegionArray = new JSONArray();
		
		JSONObject regionObject = new JSONObject();
		regionObject.put("min", genericSpawnObject);
		regionObject.put("max", genericSpawnObject);
		
		genericRegionArray.add(regionObject);
		genericRegionArray.add(regionObject);
		genericRegionArray.add(regionObject);
		
		mainObject.put("goodInitialSpawn", genericSpawnYawObject);
		mainObject.put("evilInitialSpawn", genericSpawnYawObject);
		
		mainObject.put("goodSpawn", genericSpawnYawObject);
		mainObject.put("evilSpawn", genericSpawnYawObject);
		
		mainObject.put("boundsMin", genericSpawnObject);
		mainObject.put("boundsMax", genericSpawnObject);
		
		mainObject.put("protected", genericRegionArray);
		mainObject.put("nonRegen", genericRegionArray);
		
		mainObject.put("flags", flagArray);
		mainObject.put("miniBlocks", armorStandArray);
		
		return mainObject.toJSONString();
	}
	
	private static JSONObject getNewJSONArmorStand(Location deltaLocation, int materialID, int materialData){
		JSONObject armorStandObject = new JSONObject();
		
		armorStandObject.put("x", deltaLocation.getX());
		armorStandObject.put("y", deltaLocation.getY());
		armorStandObject.put("z", deltaLocation.getZ());

		armorStandObject.put("mid", materialID);
		armorStandObject.put("md", materialData);

		return armorStandObject;
	}
	
	private static JSONObject getNewFlagJSONArmorStand(Location flagLocation, Location miniDeltaLocation, Location miniHitboxLocation){
		JSONObject armorStandObject = new JSONObject();
		
		armorStandObject.put("fx", flagLocation.getX());
		armorStandObject.put("fy", flagLocation.getY());
		armorStandObject.put("fz", flagLocation.getZ());
		armorStandObject.put("fYaw", flagLocation.getYaw());
		
		armorStandObject.put("miniX", miniDeltaLocation.getX());
		armorStandObject.put("miniY", miniDeltaLocation.getY());
		armorStandObject.put("miniZ", miniDeltaLocation.getZ());
		
		armorStandObject.put("hX", miniHitboxLocation.getX());
		armorStandObject.put("hY", miniHitboxLocation.getY());
		armorStandObject.put("hZ", miniHitboxLocation.getZ());
		
		return armorStandObject;
	}
	
	// only use this method for testing
	private static void createArmorStand(Player player, Location deltaLocation, int materialID, int materialData){
		PlayerConnection playerConnection = ((CraftPlayer)player).getHandle().playerConnection;
		
		EntityArmorStand armorStand = new EntityArmorStand(((CraftWorld)player.getWorld()).getHandle());
		armorStand.setInvisible(true);
		armorStand.setBasePlate(false);
		armorStand.setInvulnerable(true);
		armorStand.setNoGravity(true);
		armorStand.setSilent(true);
		
		if(materialID == Material.WOOL.getId()){
			armorStand.setSmall(true);
		}
		
		armorStand.setArms(true);
		armorStand.setRightArmPose(new Vector3f(345, 45, 0));
		
		Location newLoc = deltaLocation.clone().add(player.getLocation().add(0, 14, 0));
		
		armorStand.setPositionRotation(newLoc.getX(), newLoc.getY(), newLoc.getZ(), 0, 0);
		
		playerConnection.sendPacket(new PacketPlayOutSpawnEntityLiving(armorStand));
		
		ItemStack itemStack = new ItemStack(materialID);
		itemStack.setDurability((short)materialData);
		PacketPlayOutEntityEquipment eq = new PacketPlayOutEntityEquipment(armorStand.getId(), EnumItemSlot.MAINHAND, CraftItemStack.asNMSCopy(itemStack));
		playerConnection.sendPacket(eq);
	}
}
