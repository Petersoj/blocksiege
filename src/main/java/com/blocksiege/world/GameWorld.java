package com.blocksiege.world;

import java.util.ArrayList;

import org.bukkit.Location;
import org.bukkit.World;

import com.blocksiege.BlockSiege;
import com.blocksiege.game.Flag;
import com.blocksiege.spawn.MiniMap;
import com.blocksiege.spawn.SpawnArea;
import com.blocksiege.util.DebugUtils;
import com.blocksiege.util.HitBox;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public abstract class GameWorld {
	
	private BlockSiege plugin;
	
	private World world;
	private SpawnArea goodSpawn, evilSpawn;
	private MapTeam goodTeam, evilTeam;
	private HitBox mapBounds;
	private ArrayList<HitBox> protectedRegions;
	private ArrayList<HitBox> nonRegenRegions;
	
	private Flag[] flags;
	
	public GameWorld(BlockSiege plugin, World world, String worldAssetName){
		this.plugin = plugin;
		this.world = world;
		
		this.goodTeam = new MapTeam(this);
		this.evilTeam = new MapTeam(this);
		
		this.protectedRegions = new ArrayList<HitBox>();
		this.nonRegenRegions = new ArrayList<HitBox>();
		
		this.flags = new Flag[10];

		this.setupTeams();
		this.loadWorldData(worldAssetName);
	}
	
	public abstract void setupTeams();
	
	private void loadWorldData(String worldAssetName){
		try {
			String stringData = plugin.getDataController().readAssetFile(worldAssetName);
			JsonObject worldData = new JsonParser().parse(stringData).getAsJsonObject();
			
			// Good and evil Spawn
			JsonObject goodSpawn = worldData.getAsJsonObject("goodSpawn");
			JsonObject evilSpawn = worldData.getAsJsonObject("evilSpawn");
			Location goodSpawnLoc = new Location(world, goodSpawn.get("x").getAsInt(), goodSpawn.get("y").getAsInt(), goodSpawn.get("z").getAsInt(),
					goodSpawn.get("yaw").getAsInt(), 0);
			Location evilSpawnLoc = new Location(world, evilSpawn.get("x").getAsInt(), evilSpawn.get("y").getAsInt(), evilSpawn.get("z").getAsInt(),
					evilSpawn.get("yaw").getAsInt(), 0);
			
			// Good and evil Initial Spawn
			JsonObject goodInitialSpawn = worldData.getAsJsonObject("goodInitialSpawn");
			JsonObject evilInitialSpawn = worldData.getAsJsonObject("evilInitialSpawn");
			Location goodInitialSpawnLoc = new Location(world, goodInitialSpawn.get("x").getAsInt(), goodInitialSpawn.get("y").getAsInt(),
					goodInitialSpawn.get("z").getAsInt(), goodInitialSpawn.get("yaw").getAsInt(), 0);
			Location evilInitialSpawnLoc = new Location(world, evilInitialSpawn.get("x").getAsInt(), evilInitialSpawn.get("y").getAsInt(),
					evilInitialSpawn.get("z").getAsInt(), evilInitialSpawn.get("yaw").getAsInt(), 0);
			
			// Construct MiniMap and spawnAreas
			JsonArray miniBlocksArray = worldData.getAsJsonArray("miniBlocks");
			JsonArray flagArray = worldData.getAsJsonArray("flags");

			
			
			this.goodSpawn = new SpawnArea(this, goodTeam, goodSpawnLoc, goodInitialSpawnLoc);
			this.evilSpawn = new SpawnArea(this, evilTeam, evilSpawnLoc, evilInitialSpawnLoc);
			
			MiniMap goodMiniMap = new MiniMap(this, goodTeam, this.goodSpawn.getMiniMapLocation(), miniBlocksArray, flagArray);
			MiniMap evilMiniMap = new MiniMap(this, evilTeam, this.evilSpawn.getMiniMapLocation(), miniBlocksArray, flagArray);
			
			this.goodSpawn.setMiniMap(goodMiniMap);
			this.evilSpawn.setMiniMap(evilMiniMap);
			
			// Read flag data and populate flag array
			for(int index = 0; index < flagArray.size(); index++){
				JsonObject flagObject = flagArray.get(index).getAsJsonObject();
				Location flagLocation = new Location(world, flagObject.get("fx").getAsInt(), flagObject.get("fy").getAsInt(),
						flagObject.get("fz").getAsInt(), flagObject.get("fYaw").getAsInt(), 0);
				Location miniHitboxLocation = new Location(world, flagObject.get("hX").getAsInt(), flagObject.get("hY").getAsInt(),
						flagObject.get("hZ").getAsInt());
				flags[index] = new Flag(this, flagLocation, miniHitboxLocation);
			}
			
			// Add map bounds
			JsonObject boundsMin = worldData.getAsJsonObject("boundsMin");
			JsonObject boundsMax = worldData.getAsJsonObject("boundsMax");
			this.mapBounds = new HitBox(boundsMin.get("x").getAsInt(), boundsMin.get("y").getAsInt(), boundsMin.get("z").getAsInt(),
					boundsMax.get("x").getAsInt(), boundsMax.get("y").getAsInt(),boundsMax.get("z").getAsInt());
			
			// Add protected regions
			JsonArray protectedArray = worldData.getAsJsonArray("protected");
			for(JsonElement regionElement : protectedArray){
				JsonObject region = regionElement.getAsJsonObject();
				JsonObject min = region.get("min").getAsJsonObject();
				JsonObject max = region.get("max").getAsJsonObject();
				protectedRegions.add(new HitBox(min.get("x").getAsInt(), min.get("y").getAsInt(), min.get("z").getAsInt(),
					max.get("x").getAsInt(), max.get("y").getAsInt(), max.get("z").getAsInt()));
			}

			// Add non-regen regions
			JsonArray nonRegenArray = worldData.getAsJsonArray("nonRegen");
			for(JsonElement regionElement : nonRegenArray){
				JsonObject region = regionElement.getAsJsonObject();
				JsonObject min = region.get("min").getAsJsonObject();
				JsonObject max = region.get("max").getAsJsonObject();
				nonRegenRegions.add(new HitBox(min.get("x").getAsInt(), min.get("y").getAsInt(), min.get("z").getAsInt(),
					max.get("x").getAsInt(), max.get("y").getAsInt(), max.get("z").getAsInt()));
			}
		}catch(Exception e){
			DebugUtils.handleError(e, true);
		}
	}
	
	public BlockSiege getPlugin(){
		return plugin;
	}
	
	public World getWorld(){
		return world;
	}

	public MapTeam getGoodTeam() {
		return goodTeam;
	}

	public void setGoodTeam(MapTeam goodTeam) {
		this.goodTeam = goodTeam;
	}

	public MapTeam getEvilTeam() {
		return evilTeam;
	}

	public void setEvilTeam(MapTeam evilTeam) {
		this.evilTeam = evilTeam;
	}

	public SpawnArea getGoodSpawn() {
		return goodSpawn;
	}

	public SpawnArea getEvilSpawn() {
		return evilSpawn;
	}

	public HitBox getMapBounds() {
		return mapBounds;
	}

	public ArrayList<HitBox> getProtectedRegions() {
		return protectedRegions;
	}

	public ArrayList<HitBox> getNonRegenRegions() {
		return nonRegenRegions;
	}
	
	public Flag[] getFlags(){
		return flags;
	}
}