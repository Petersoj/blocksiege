package com.blocksiege.world;

import java.util.ArrayList;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.entity.FallingBlock;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import com.blocksiege.util.MathUtils;
import com.blocksiege.util.WorldUtils;

public class Explosion {
	
	private GameWorld gameWorld;
	private Location location;
	private float radius;
	private MapTeam harmedTeam;
	private ArrayList<BlockState> blocks;
	
	public Explosion(GameWorld gameWorld, Location location, MapTeam harmedTeam, float radius){
		this.gameWorld = gameWorld;
		this.location = location;
		this.harmedTeam = harmedTeam;
		this.radius = radius;
		this.blocks = new ArrayList<BlockState>();
	}
	
	public void explode(){
		removeAndCreateFallingBlocks();
		harmTeam();
		createParticleEffect();
		startBlockRegen();
	}
	
	private void removeAndCreateFallingBlocks(){
		World world = location.getWorld();
		
		// Don't remove any blocks if explosion was in water or lava.
		Material locationType = location.getBlock().getType();
		if(locationType == Material.WATER || locationType == Material.STATIONARY_WATER 
				|| locationType == Material.LAVA || locationType == Material.STATIONARY_LAVA){
			return;
		}
		// We remove all the blocks first to prevent falling block colliding into "to-be removed" blocks
		
		ArrayList<Location> sphereLocations = WorldUtils.getSphereLocations(location, radius, false, true, 0.9f);
		
		// Add blockState to list and remove block
		for(Location sphereLocation : sphereLocations){
			Block currentBlock = world.getBlockAt(sphereLocation);
			
			if(currentBlock != null && (currentBlock.getType() != Material.AIR && 
					currentBlock.getType() != Material.BEDROCK && currentBlock.getType() != Material.BARRIER)){
				Block belowBlock = currentBlock.getRelative(BlockFace.DOWN);
				
				if(belowBlock != null && belowBlock.getType() != Material.BEDROCK && !(belowBlock.getLocation().getY() < 1)){
					
					// TODO Check if in non-regen regions or protected region in GameWorld.
					// Add BlockState to gameworld list of regen-at-end-game.
					// Also check if block was outside the world bounds
					
					blocks.add(currentBlock.getState());
					currentBlock.setType(Material.AIR, false);
				}
			}
		}
		
		// Spawn falling blocks with Velocity
		for(BlockState blockState : blocks){
			if(blockState.getType().isBlock() && MathUtils.getRandomInt(1, 10) <= 3){ // Only spawn falling blocks for blocks in 30%
				Location blockLocation = blockState.getLocation();
				
				FallingBlock fallingBlock = (FallingBlock) world.spawnFallingBlock(blockLocation, blockState.getData());
				fallingBlock.setDropItem(false);
				fallingBlock.setHurtEntities(false);
				fallingBlock.setInvulnerable(true);
				fallingBlock.setSilent(true);
				fallingBlock.setVelocity(getNewFallingBlockVector(blockLocation));
			}
		}
	}
	
	private void harmTeam(){
		// TODO Implement after MapTeam implementation
	}
	
	private void createParticleEffect(){
		World world = location.getWorld();
		
		world.playSound(location, Sound.ENTITY_GENERIC_EXPLODE, SoundCategory.BLOCKS, (float) (radius / 2), (float) MathUtils.getRandomDouble(0.6, 0.95));
		
		int amountAdded = (int) (radius * radius) / 2;
		world.spawnParticle(Particle.FLAME, location, 60 + amountAdded, 0, 0, 0, (double) radius / 60);
		world.spawnParticle(Particle.SMOKE_LARGE, location, 20 + amountAdded, 0, 0, 0, (double) radius / 45);
		world.spawnParticle(Particle.SMOKE_NORMAL, location, 40 + amountAdded, 0, 0, 0, (double) radius / 35);
		world.spawnParticle(Particle.CLOUD, location, 70 + amountAdded, 0, 0, 0, (double) radius / 20);
	}
	
	private void startBlockRegen(){
		new BukkitRunnable() {
			int currentIndex = 0;
			
			public void run(){
				if(currentIndex == blocks.size()){
					for(BlockState blockState : blocks){ // Do a double check to ensure that all blocks were update properly
						blockState.update(true, false);
					}
					this.cancel();
				}else{
					if(MathUtils.getRandomInt(1, 10) < 7){
						BlockState blockState = blocks.get(currentIndex++);
						blockState.update(true, false);
						location.getWorld().playEffect(blockState.getLocation(), Effect.STEP_SOUND, blockState.getTypeId());
					}
				}
			}
		}.runTaskTimer(harmedTeam.getGameWorld().getPlugin(), 120, 5); // delay: 6 seconds, repeat: 1/4 second
	}
	
	private Vector getNewFallingBlockVector(Location blockLocation){
		Vector blockDirection = blockLocation.toVector().subtract(location.toVector());
		
		double divide = radius / blockDirection.lengthSquared();
		blockDirection.divide(new Vector(divide, divide, divide));
		
		blockDirection.setY(Math.abs(blockDirection.getY()));
		
		return blockDirection.normalize();
	}
}
