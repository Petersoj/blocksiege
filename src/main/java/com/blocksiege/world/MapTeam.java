package com.blocksiege.world;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class MapTeam {
	
	public static ItemStack DEFAULT_WOOL = new ItemStack(Material.WOOL);
	public static ItemStack DEFAULT_FENCE = new ItemStack(Material.BIRCH_FENCE);
	
	private GameWorld gameWorld;
	
	private String teamName;
	private ChatColor teamChatColor;
	double colorR, colorB, colorG;
	private ItemStack teamFlag;
	private ItemStack flagFence;
	private ItemStack flagWool;
	private ItemStack teamHelmet;
	
	public MapTeam(GameWorld gameWorld){
		this.gameWorld = gameWorld;
	}
	
	public GameWorld getGameWorld(){
		return gameWorld;
	}
	
	public String getTeamName(){
		return teamName;
	}
	
	public void setTeamName(String teamName){
		this.teamName = teamName;
	}

	public ChatColor getTeamChatColor() {
		return teamChatColor;
	}

	public void setTeamChatColor(ChatColor teamChatColor) {
		this.teamChatColor = teamChatColor;
	}

	public double getTeamColorRed() {
		return colorR;
	}
	
	public double getTeamColorGreen(){
		return colorG;
	}
	
	public double getTeamColorBlue(){
		return colorB;
	}

	public void setTeamColor(double r, double g, double b) {
		this.colorR = r;
		this.colorG = b;
		this.colorB = b;
	}

	public ItemStack getTeamFlag() {
		return teamFlag;
	}

	public void setTeamFlag(ItemStack teamFlag) {
		this.teamFlag = teamFlag;
	}

	public ItemStack getFlagFence() {
		return flagFence;
	}

	public void setFlagFence(ItemStack flagFence) {
		this.flagFence = flagFence;
	}

	public ItemStack getFlagWool() {
		return flagWool;
	}

	public void setFlagWool(ItemStack flagWool) {
		this.flagWool = flagWool;
	}

	public ItemStack getTeamHelmet() {
		return teamHelmet;
	}

	public void setTeamHelmet(ItemStack teamHelmet) {
		this.teamHelmet = teamHelmet;
	}
}
