package com.blocksiege.world.worlds;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.WorldCreator;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;

import com.blocksiege.BlockSiege;
import com.blocksiege.world.GameWorld;
import com.blocksiege.world.MapTeam;

public class HelmsDeepWorld extends GameWorld {

	public HelmsDeepWorld(BlockSiege plugin) {
		super(plugin, Bukkit.createWorld(new WorldCreator("world_helms_deep")), "data_helms_deep.json");
	}

	@Override
	public void setupTeams() {
		// Good Team
		MapTeam goodTeam = super.getGoodTeam();
		goodTeam.setTeamName("Humans");
		goodTeam.setTeamChatColor(ChatColor.AQUA);
		goodTeam.setTeamColor(85D / 255D, 1D, 1D);
		
		// Banner
		ItemStack goodTeamFlag = new ItemStack(Material.BANNER);
		BannerMeta goodBannerMeta = (BannerMeta) goodTeamFlag.getItemMeta();
		
		goodBannerMeta.setBaseColor(DyeColor.LIGHT_BLUE);
		goodBannerMeta.addPattern(new Pattern(DyeColor.BLUE, PatternType.GRADIENT));
		goodBannerMeta.addPattern(new Pattern(DyeColor.WHITE, PatternType.FLOWER));
		
		goodTeamFlag.setItemMeta(goodBannerMeta);
		
		goodTeam.setTeamFlag(goodTeamFlag);
		goodTeam.setFlagFence(new ItemStack(Material.FENCE)); // Oak fence
		goodTeam.setFlagWool(new ItemStack(Material.WOOL, 1, (short) 0, Byte.valueOf((byte) 3)));
		//goodTeam.setTeamHelmet(); // This kits will set the helmet for this team. 
		
		
		// Evil Team
		MapTeam evilTeam = super.getGoodTeam();
		evilTeam.setTeamName("Urkai");
		evilTeam.setTeamChatColor(ChatColor.RED);
		evilTeam.setTeamColor(1, 85D / 255D, 85D / 255D);
		
		// Banner
		ItemStack evilTeamFlag = new ItemStack(Material.BANNER);
		BannerMeta evilBannerMeta = (BannerMeta) evilTeamFlag.getItemMeta();
		
		evilBannerMeta.setBaseColor(DyeColor.RED);
		goodBannerMeta.addPattern(new Pattern(DyeColor.BLACK, PatternType.SKULL));
		goodBannerMeta.addPattern(new Pattern(DyeColor.BLACK, PatternType.GRADIENT));
		
		evilTeamFlag.setItemMeta(evilBannerMeta);
		
		evilTeam.setTeamFlag(evilTeamFlag);
		evilTeam.setFlagFence(new ItemStack(Material.DARK_OAK_FENCE));
		evilTeam.setFlagWool(new ItemStack(Material.WOOL, 1, (short) 0, Byte.valueOf((byte) 14)));
		
		ItemStack evilTeamHelmet = new ItemStack(Material.SKULL_ITEM, 1, (short) 0, Byte.valueOf((byte) 2));
		evilTeam.setTeamHelmet(evilTeamHelmet);
	}

}