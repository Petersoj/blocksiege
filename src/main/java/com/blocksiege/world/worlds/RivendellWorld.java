package com.blocksiege.world.worlds;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.WorldCreator;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;

import com.blocksiege.BlockSiege;
import com.blocksiege.world.GameWorld;
import com.blocksiege.world.MapTeam;

public class RivendellWorld extends GameWorld {

	public RivendellWorld(BlockSiege plugin) {
		super(plugin, Bukkit.createWorld(new WorldCreator("world_rivendell")), "data_rivendell.json");
	}

	@Override
	public void setupTeams() {
		// Good Team
		MapTeam goodTeam = super.getGoodTeam();
		goodTeam.setTeamName("Elf");
		goodTeam.setTeamChatColor(ChatColor.YELLOW);
		goodTeam.setTeamColor(1D, 1D, 85D / 255D);
		
		// Banner
		ItemStack goodTeamFlag = new ItemStack(Material.BANNER);
		BannerMeta goodBannerMeta = (BannerMeta) goodTeamFlag.getItemMeta();
		
		goodBannerMeta.setBaseColor(DyeColor.WHITE);
		goodBannerMeta.addPattern(new Pattern(DyeColor.YELLOW, PatternType.FLOWER));
		goodBannerMeta.addPattern(new Pattern(DyeColor.WHITE, PatternType.CIRCLE_MIDDLE));
		goodBannerMeta.addPattern(new Pattern(DyeColor.YELLOW, PatternType.TRIANGLES_BOTTOM));
		goodBannerMeta.addPattern(new Pattern(DyeColor.YELLOW, PatternType.TRIANGLES_TOP));
		goodBannerMeta.addPattern(new Pattern(DyeColor.WHITE, PatternType.CURLY_BORDER));
		
		goodTeamFlag.setItemMeta(goodBannerMeta);
		
		goodTeam.setTeamFlag(goodTeamFlag);
		goodTeam.setFlagFence(new ItemStack(Material.BIRCH_FENCE));
		goodTeam.setFlagWool(new ItemStack(Material.WOOL, 1, (short) 0, Byte.valueOf((byte) 4)));
		//goodTeam.setTeamHelmet(); // This kits will set the helmet for this team. 
		
		
		// Evil Team
		MapTeam evilTeam = super.getGoodTeam();
		evilTeam.setTeamName("Guldur");
		evilTeam.setTeamChatColor(ChatColor.DARK_RED);
		evilTeam.setTeamColor(170D, 0, 0);
		
		// Banner
		ItemStack evilTeamFlag = new ItemStack(Material.BANNER);
		BannerMeta evilBannerMeta = (BannerMeta) evilTeamFlag.getItemMeta();
		
		evilBannerMeta.setBaseColor(DyeColor.RED);
		goodBannerMeta.addPattern(new Pattern(DyeColor.BLACK, PatternType.GRADIENT_UP));
		goodBannerMeta.addPattern(new Pattern(DyeColor.BLACK, PatternType.CURLY_BORDER));
		goodBannerMeta.addPattern(new Pattern(DyeColor.BLACK, PatternType.CREEPER));
		
		evilTeamFlag.setItemMeta(evilBannerMeta);
		
		evilTeam.setTeamFlag(evilTeamFlag);
		evilTeam.setFlagFence(new ItemStack(Material.DARK_OAK_FENCE));
		evilTeam.setFlagWool(new ItemStack(Material.WOOL, 1, (short) 0, Byte.valueOf((byte) 15)));
		
		ItemStack evilTeamHelmet = new ItemStack(Material.SKULL_ITEM, 1, (short) 0, Byte.valueOf((byte) 2));
		evilTeam.setTeamHelmet(evilTeamHelmet);
	}
}
