package com.blocksiege.world.worlds;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.WorldCreator;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;

import com.blocksiege.BlockSiege;
import com.blocksiege.world.GameWorld;
import com.blocksiege.world.MapTeam;

public class RohanWorld extends GameWorld {
	
	public RohanWorld(BlockSiege plugin) {
		super(plugin, Bukkit.createWorld(new WorldCreator("world_rohan")), "data_rohan.json");
	}

	@Override
	public void setupTeams() {
		// Good Team
		MapTeam goodTeam = super.getGoodTeam();
		goodTeam.setTeamName("Rohan");
		goodTeam.setTeamChatColor(ChatColor.DARK_GREEN);
		goodTeam.setTeamColor(0.0001, 170D, 0);
		
		// Banner
		ItemStack goodTeamFlag = new ItemStack(Material.BANNER);
		BannerMeta goodBannerMeta = (BannerMeta) goodTeamFlag.getItemMeta();
		
		goodBannerMeta.setBaseColor(DyeColor.GREEN);
		goodBannerMeta.addPattern(new Pattern(DyeColor.WHITE, PatternType.STRAIGHT_CROSS));
		goodBannerMeta.addPattern(new Pattern(DyeColor.GREEN, PatternType.STRIPE_TOP));
		goodBannerMeta.addPattern(new Pattern(DyeColor.WHITE, PatternType.TRIANGLES_TOP));
		goodBannerMeta.addPattern(new Pattern(DyeColor.WHITE, PatternType.FLOWER));
		goodBannerMeta.addPattern(new Pattern(DyeColor.WHITE, PatternType.TRIANGLES_BOTTOM));
		goodBannerMeta.addPattern(new Pattern(DyeColor.WHITE, PatternType.BORDER));
		
		goodTeamFlag.setItemMeta(goodBannerMeta);
		
		goodTeam.setTeamFlag(goodTeamFlag);
		goodTeam.setFlagFence(new ItemStack(Material.FENCE)); // Oak fence
		goodTeam.setFlagWool(new ItemStack(Material.WOOL, 1, (short) 0, Byte.valueOf((byte) 13)));
		//goodTeam.setTeamHelmet(); // This kits will set the helmet for this team. 
		
		
		// Evil Team
		MapTeam evilTeam = super.getGoodTeam();
		evilTeam.setTeamName("Gurk");
		evilTeam.setTeamChatColor(ChatColor.DARK_GRAY);
		evilTeam.setTeamColor(85D, 85D, 85D);
		
		// Banner
		ItemStack evilTeamFlag = new ItemStack(Material.BANNER);
		BannerMeta evilBannerMeta = (BannerMeta) evilTeamFlag.getItemMeta();
		
		evilBannerMeta.setBaseColor(DyeColor.RED);
		goodBannerMeta.addPattern(new Pattern(DyeColor.BLACK, PatternType.STRIPE_SMALL));
		goodBannerMeta.addPattern(new Pattern(DyeColor.RED, PatternType.STRIPE_CENTER));
		goodBannerMeta.addPattern(new Pattern(DyeColor.BLACK, PatternType.TRIANGLES_BOTTOM));
		goodBannerMeta.addPattern(new Pattern(DyeColor.BLACK, PatternType.BORDER));
		goodBannerMeta.addPattern(new Pattern(DyeColor.BLACK, PatternType.TRIANGLE_TOP));
		goodBannerMeta.addPattern(new Pattern(DyeColor.BLACK, PatternType.STRIPE_MIDDLE));
		
		evilTeamFlag.setItemMeta(evilBannerMeta);
		
		evilTeam.setTeamFlag(evilTeamFlag);
		evilTeam.setFlagFence(new ItemStack(Material.DARK_OAK_FENCE));
		evilTeam.setFlagWool(new ItemStack(Material.WOOL, 1, (short) 0, Byte.valueOf((byte) 15)));
		
		ItemStack evilTeamHelmet = new ItemStack(Material.SKULL_ITEM, 1, (short) 0, Byte.valueOf((byte) 2));
		evilTeam.setTeamHelmet(evilTeamHelmet);
	}

}
