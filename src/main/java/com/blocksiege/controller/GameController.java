package com.blocksiege.controller;

import com.blocksiege.BlockSiege;

public class GameController {
	
	private BlockSiege plugin;
	
	public GameController(BlockSiege plugin){
		this.plugin = plugin;
	}
	
	public void start(){
		
	}
	
	public BlockSiege getPlugin(){
		return plugin;
	}

}
