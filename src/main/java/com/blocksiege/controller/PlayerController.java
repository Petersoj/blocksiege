package com.blocksiege.controller;

import java.util.HashMap;
import java.util.UUID;

import com.blocksiege.BlockSiege;
import com.blocksiege.player.GamePlayer;
import com.blocksiege.player.ranks.AdminRank;
import com.blocksiege.player.ranks.DefaultRank;
import com.blocksiege.player.ranks.KnightRank;
import com.blocksiege.player.ranks.ModRank;
import com.blocksiege.player.ranks.OwnerRank;

public class PlayerController {
	
	private BlockSiege plugin;
	
	private final HashMap<UUID, GamePlayer> gamePlayers;
	
	private final DefaultRank defaultRank;
	private final KnightRank knightRank;
	private final ModRank modRank;
	private final AdminRank adminRank;
	private final OwnerRank ownerRank;
	
	public PlayerController(BlockSiege plugin){
		this.plugin = plugin;
		
		this.gamePlayers = new HashMap<UUID, GamePlayer>();
		
		this.defaultRank = new DefaultRank();
		this.knightRank = new KnightRank();
		this.modRank = new ModRank();
		this.adminRank = new AdminRank();
		this.ownerRank = new OwnerRank();
	}
	
	public GamePlayer getGamePlayer(UUID uuid){
		return gamePlayers.get(uuid);
	}

	public DefaultRank getDefaultRank() {
		return defaultRank;
	}

	public KnightRank getKnightRank() {
		return knightRank;
	}

	public ModRank getModRank() {
		return modRank;
	}

	public AdminRank getAdminRank() {
		return adminRank;
	}

	public OwnerRank getOwnerRank() {
		return ownerRank;
	}
}
