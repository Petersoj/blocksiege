package com.blocksiege.controller;

import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.UUID;

import com.blocksiege.BlockSiege;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class DataController {
	
	private final HikariDataSource dataSource;
	
	private final String playerDataQuery = "CALL on_join_player_data(?);";
	
	private final String updatePrefix = "UPDATE player_data SET ";
	private final String updateSuffix = " WHERE uuid=?;";
	
	private String playerDataUpdate = "coins=coins+?,xp=xp+?,kills=kills+?,deaths=deaths+?,wins=wins+?,"
			+ "losses=losses+?,flags_captured=flags_captured+?,time_played=time_played+?";
	private final String playerDataKitUpdate = "kits=?";
	private final String playerDataSetRank = "rank=?";

	public DataController(BlockSiege plugin) {

		// The MySQL Server will only be avaliable on the loopback interface with ipconfig so we don't have to do a ton of security management.
		HikariConfig config = new HikariConfig(); // Most of the defaults are good here and no need to change them.
		config.setJdbcUrl("jdbc:mysql://" + plugin.getConfig().getString("mysql-host") 
				+ "/" + plugin.getConfig().getString("mysql-database") + "?useSSL=true");
		config.setUsername(plugin.getConfig().getString("mysql-username"));
		config.setPassword(plugin.getConfig().getString("mysql-password"));

		dataSource = new HikariDataSource(config);
	}
	
	public HikariDataSource getHikariDataSource(){
		return dataSource;
	}
	
	public void preparePlayerDataQuery(PreparedStatement statement, UUID uuid) throws SQLException {
		statement.setString(1, uuid.toString());
	}
	
	public void preparePlayerDataUpdate(PreparedStatement statement, UUID uuid, int coins, int xp, int kills, int deaths,
			int wins, int losses, int flags, int time) throws SQLException{
		statement.setInt(1, coins);
		statement.setInt(2, xp);
		statement.setInt(3, kills);
		statement.setInt(4, deaths);
		statement.setInt(5, wins);
		statement.setInt(6, losses);
		statement.setInt(7, flags);
		statement.setInt(8, time);
		statement.setString(9, uuid.toString());
	}
	
	public void preparePlayerDataUpdateWithKits(PreparedStatement statement, UUID uuid, String kits, int coins, int xp, int kills, int deaths,
			int wins, int losses, int flags, int time) throws SQLException {
		statement.setString(1, kits);
		statement.setInt(2, coins);
		statement.setInt(3, xp);
		statement.setInt(4, kills);
		statement.setInt(5, deaths);
		statement.setInt(6, wins);
		statement.setInt(7, losses);
		statement.setInt(8, flags);
		statement.setInt(9, time);
		statement.setString(10, uuid.toString());
	}
	
	public String getPlayerDataQuery(){
		return playerDataQuery;
	}
	
	public String getPlayerDataUpdateQuery(){
		return updatePrefix + playerDataUpdate + updateSuffix;
	}
	
	public String getPlayerDataUpdateWithKits(){
		return updatePrefix + playerDataKitUpdate + "," + playerDataUpdate + updateSuffix;
	}
	
	public String getPlayerDataSetRank(){
		return updatePrefix + playerDataSetRank + updateSuffix;
	}
	
	public String readAssetFile(String fileName){ // Standard Java way of reading a file from class path.
		int bufferSize = 8192; // 8 KB
		char[] buffer = new char[bufferSize];
		
		StringBuilder strings = new StringBuilder();
		
		Reader reader = new InputStreamReader(getClass().getResourceAsStream("/com/blocksiege/assets/" + fileName));
		int read = 0;
		try{
			while((read = reader.read(buffer, 0, bufferSize)) != -1){
				strings.append(buffer, 0, read);
			}
		}catch(Exception ignored){}
		return strings.toString();
	}

	/*
	 -Create Database-
	 create database player_data;
	 
	 -Create Table-
	 create table player_data (uuid CHAR(36), rank VARCHAR(10), coins INT, kits VARCHAR(20), xp INT,
		kills INT, deaths INT, wins INT, losses INT, flags_captured INT, time_played INT UNSIGNED, PRIMARY KEY (uuid));
	 
	 -Create Procedure-
		DELIMITER //
		
		create procedure on_join_player_data(IN player_uuid CHAR(36)) 
		begin  IF EXISTS (SELECT * FROM player_data WHERE uuid = player_uuid) THEN 
		  SELECT * FROM player_data WHERE uuid = player_uuid; ELSE 
		    INSERT INTO player_data VALUES (player_uuid, 'default', 0, '0-0-0-0', 0, 0, 0, 0, 0, 0, 0);
		       SELECT * FROM player_data WHERE uuid = player_uuid; END IF;  end//
		
		DELIMITER ;
	 
	 */
}