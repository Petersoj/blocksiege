package com.blocksiege.listener;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.block.Block;
import org.bukkit.entity.FallingBlock;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockFadeEvent;
import org.bukkit.event.block.BlockFormEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockSpreadEvent;
import org.bukkit.event.block.LeavesDecayEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.entity.SlimeSplitEvent;
import org.bukkit.event.entity.SpawnerSpawnEvent;
import org.bukkit.event.player.PlayerArmorStandManipulateEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;
import org.bukkit.event.world.StructureGrowEvent;

import com.blocksiege.BlockSiege;

public class Listeners implements Listener {
	
	private BlockSiege plugin;
	
	public Listeners(BlockSiege plugin){
		this.plugin = plugin;
		
		Bukkit.getPluginManager().registerEvents(this, plugin);
	}
	
	// SERVER EVENTS
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e){
		
	}
	
	// GAME RELATED EVENTS
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e){
		
	}
	
	
	// Future listeners
	//
	/*
	 * ItemSpawnEvent - Tile entity?
	 * entitySpawnEvent - no monsters
	 * BlockPlaceEvent
	 * BlockBreakEvent
	 * BlockDamageEvent
	 * PlayerDropItemEvent
	 * PlayerItemBreakEvent
	 * PlayerItemDamageEvent - maybe.
	 * PlayerItemHeldEvent
	 * PlayerPickupArrowEvent
	 * PlayerPickupItemEvent
	 * 
	 * PlayerToggleSneakEvent
	 * PlayerSpringEvent
	 * FoodLevelChangeEvent
	 * AsyncPlayerChatEvent
	 * 
	 * EntitySpawnEvent
	 * EntityShootBowEvent || ProjectileLaunchEvent
	 * ProjectileHitEvent
	 * 
	 * StructureGrowEvent
	 * 
	 * ANY VEHICLE EVENTS FOR RIDING HORSES 
	 * 
	 * InventoryClickEvent
	 */
	
	// OTHER EVENTS (mostly cancel events)
	
	@EventHandler
	public void onItemSpawn(ItemSpawnEvent e){
		// TODO Check if need to cancel this after mostly done with server
	}
	
	@EventHandler
	public void onBlockExplode(BlockExplodeEvent e){
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onEntityExplode(EntityExplodeEvent e){
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onArmorStandManipulate(PlayerArmorStandManipulateEvent e){
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onPlayerBucketEmpty(PlayerBucketEmptyEvent e){
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onSwapHandItems(PlayerSwapHandItemsEvent e){
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onBlockBurn(BlockBurnEvent e){
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onBlockDamage(BlockDamageEvent e){
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onBlockFade(BlockFadeEvent e){
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onBlockForm(BlockFormEvent e){
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onBlockSpread(BlockSpreadEvent e){
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onBlockIgnite(BlockIgniteEvent e){
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onLeaveDecay(LeavesDecayEvent e){
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onStructureGrow(StructureGrowEvent e){
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onSpawnerSpawn(SpawnerSpawnEvent e){
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onSlimeSplit(SlimeSplitEvent e){
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onEntityChangeBlock(EntityChangeBlockEvent e){
		Block block = e.getBlock();
		block.getWorld().playEffect(block.getLocation(), Effect.STEP_SOUND, ((FallingBlock)e.getEntity()).getBlockId());
		e.setCancelled(true);
	}
}
