package com.blocksiege.spawn;

import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_11_R1.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;

import com.blocksiege.player.GamePlayer;
import com.blocksiege.util.NMSUtils;
import com.blocksiege.world.GameWorld;
import com.blocksiege.world.MapTeam;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import net.minecraft.server.v1_11_R1.EntityArmorStand;
import net.minecraft.server.v1_11_R1.EnumItemSlot;

public class MiniMap {
	
	private Location location;
	private EntityArmorStand[] miniBlocks;
	private EntityArmorStand[] miniFlags;

	public MiniMap(GameWorld gameWorld, MapTeam mapTeam, Location miniMapLocation, JsonArray miniBlocksArray, JsonArray miniFlagsArray){
		this.location = miniMapLocation;
		
		this.miniBlocks = new EntityArmorStand[miniBlocksArray.size()];
		this.miniFlags = new EntityArmorStand[miniFlagsArray.size()];
		
		this.createMiniBlocks(gameWorld, miniMapLocation, miniBlocksArray);
		this.createMiniFlags(gameWorld, miniMapLocation, miniFlagsArray);
	}
	
	private void createMiniBlocks(GameWorld gameWorld, Location miniMapLocation, JsonArray miniBlocksArray){
		for(int index = 0; index < miniBlocksArray.size(); index++){
			
			JsonObject armorObject = miniBlocksArray.get(index).getAsJsonObject();
			EntityArmorStand miniArmor = NMSUtils.getNewArmorStand(gameWorld);
			
			miniArmor.setPositionRotation(miniMapLocation.getX() + armorObject.get("x").getAsDouble(),
					miniMapLocation.getY() + armorObject.get("y").getAsDouble(),
					miniMapLocation.getZ() + armorObject.get("z").getAsDouble(), 0, 0);
			
			ItemStack miniItem = new ItemStack(armorObject.get("mid").getAsInt());
			miniItem.setDurability(armorObject.get("md").getAsShort());
			miniArmor.setSlot(EnumItemSlot.MAINHAND, CraftItemStack.asNMSCopy(miniItem));
			
			miniBlocks[index] = miniArmor;
		}
	}
	
	private void createMiniFlags(GameWorld gameWorld, Location miniMapLocation, JsonArray miniFlagsArray){
		for(int index = 0; index < miniFlagsArray.size(); index++){
			
			JsonObject flagData = miniFlagsArray.get(index).getAsJsonObject();
			EntityArmorStand miniArmor = NMSUtils.getNewArmorStand(gameWorld);
			
			miniArmor.setSmall(true);
			miniArmor.setPositionRotation(miniMapLocation.getX() + flagData.get("miniX").getAsDouble(),
					miniMapLocation.getY() + flagData.get("miniY").getAsDouble(),
					miniMapLocation.getZ() + flagData.get("miniZ").getAsDouble(), 0, 0);
			miniArmor.setSlot(EnumItemSlot.MAINHAND, CraftItemStack.asNMSCopy(MapTeam.DEFAULT_WOOL));
			
			miniFlags[index] = miniArmor;
		}
	}
	
	public void setMiniFlagWool(MapTeam mapTeam){
		
	}
	
	public void spawnMiniMap(GamePlayer gamePlayer){
		
	}
	
	public Location getLocation(){
		return location;
	}
	
	public EntityArmorStand[] getMiniBlocks(){
		return miniBlocks;
	}
	
	public EntityArmorStand[] getMiniFlags(){
		return miniFlags;
	}
}