package com.blocksiege.spawn;

import org.bukkit.ChatColor;
import org.bukkit.Location;

import com.blocksiege.util.NMSUtils;
import com.blocksiege.world.GameWorld;
import com.blocksiege.world.MapTeam;

import net.minecraft.server.v1_11_R1.EntityArmorStand;

public class SpawnArea {
	
	private GameWorld gameWorld;
	
	private MapTeam mapTeam;
	private MiniMap miniMap;
	
	private Location spawnLocation;
	private Location initialSpawnLocation;
	
	private Location miniMapLocation;
	private Location swordsmanLocation, archerLocation, scoutLocation, mageLocation;
	private Location statsLocation;

	private EntityArmorStand statsSign;
	private EntityArmorStand miniMapInfo1;
	private EntityArmorStand miniMapInfo2;
	
	public SpawnArea(GameWorld gameWorld, MapTeam mapTeam, Location spawnLocation, Location initialSpawnLocation){
		this.gameWorld = gameWorld;
		this.mapTeam = mapTeam;
		
		this.spawnLocation = spawnLocation;
		this.initialSpawnLocation = initialSpawnLocation;
		
		this.setupLocations();
		this.createArmorStands();
	}
	
	private void setupLocations(){
		this.miniMapLocation = spawnLocation.clone().add(25, 2, -2);
		this.swordsmanLocation = spawnLocation.clone().add(6, 2, -5);
		this.archerLocation = spawnLocation.clone().add(6, 2, 5);
		this.scoutLocation = spawnLocation.clone().add(10, 2, -5);
		this.mageLocation = spawnLocation.clone().add(10, 2, 5);
		this.statsLocation = spawnLocation.clone().add(22, 0, 3);
	}
	
	private void createArmorStands(){
//		this.statsSign = NMSUtils.getNewArmorStand(gameWorld);
//		statsSign.setCustomNameVisible(true);
//		statsSign.setCustomName(ChatColor.BOLD + "" + ChatColor.GOLD + "Your Stats");
//		statsSign.setPosition(statsLocation.getBlockX() + 0.5, statsLocation.getBlockY() + 1, statsLocation.getBlockZ() + 0.5);
//
//		Location miniMapLocation = miniMap.getLocation();
//		
//		this.miniMapInfo1 = NMSUtils.getNewArmorStand(gameWorld);
//		miniMapInfo1.setCustomNameVisible(true);
//		miniMapInfo1.setCustomName(ChatColor.WHITE + "You are on the " + ChatColor.BOLD + mapTeam.getTeamChatColor()
//				+ mapTeam.getTeamName() + ChatColor.WHITE + " team.");
//		miniMapInfo1.setPosition(miniMapLocation.getBlockX() + 2.5, miniMapLocation.getBlockY() + 1, miniMapLocation.getBlockZ() + 2.5);
//		
//		this.miniMapInfo2 = NMSUtils.getNewArmorStand(gameWorld);
//		miniMapInfo2.setCustomNameVisible(true);
//		miniMapInfo2.setCustomName(ChatColor.GRAY + "(Right click on a wool block to spawn)");
//		miniMapInfo2.setPosition(miniMapLocation.getBlockX() + 2.5, miniMapLocation.getBlockY() + 0.85, miniMapLocation.getBlockZ() + 2.5);
	}
	
	public GameWorld getGameWorld() {
		return gameWorld;
	}
	
	public void setMiniMap(MiniMap miniMap){
		this.miniMap = miniMap;
	}
	
	public MiniMap getMiniMap() {
		return miniMap;
	}
	
	public Location getSpawnLocation() {
		return spawnLocation;
	}

	public Location getInitialSpawnLocation() {
		return initialSpawnLocation;
	}

	public Location getMiniMapLocation(){
		return miniMapLocation;
	}
}