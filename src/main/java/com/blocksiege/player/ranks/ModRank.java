package com.blocksiege.player.ranks;

import java.util.ArrayList;

import org.bukkit.ChatColor;

public class ModRank extends DefaultRank {
	
	public ModRank(){
		super.setRankName("MOD");
		super.setRankColor(ChatColor.GREEN);
	}
	
	@Override
	public void addCommands() {
		super.addCommands();
		ArrayList<String> commands = super.getCommands();
		commands.add("/tempban");
		commands.add("/tempmute");
		commands.add("/viewreports");
		commands.add("/reportfirm");
		commands.add("/spectate");
	}
}