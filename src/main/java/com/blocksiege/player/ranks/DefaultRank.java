package com.blocksiege.player.ranks;

import java.util.ArrayList;

import org.bukkit.ChatColor;

import com.blocksiege.player.Rank;

public class DefaultRank extends Rank {
	
	public DefaultRank(){
		super.setRankName("DEFAULT");
		super.setRankColor(ChatColor.YELLOW);
	}

	@Override
	public void addCommands() {
		ArrayList<String> currentCommands = super.getCommands();
		currentCommands.add("/kill");
		currentCommands.add("/suicide");
		currentCommands.add("/die");
		currentCommands.add("/help");
		// currentCommands.add("/flags"); Implement Later
		// currentCommands.add("/stats"); Implement Later
		currentCommands.add("/report");
	}
}
