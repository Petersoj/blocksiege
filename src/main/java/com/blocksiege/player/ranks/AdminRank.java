package com.blocksiege.player.ranks;

import java.util.ArrayList;

import org.bukkit.ChatColor;

public class AdminRank extends ModRank {
	
	public AdminRank(){
		super.setRankName("ADMIN");
		super.setRankColor(ChatColor.GOLD);
	}
	
	@Override
	public void addCommands() {
		super.addCommands();
		ArrayList<String> commands = super.getCommands();
		commands.add("/permban");
		commands.add("/permmute");
	}

}
