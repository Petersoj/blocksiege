package com.blocksiege.player.ranks;

import java.util.ArrayList;

import org.bukkit.ChatColor;

public class OwnerRank extends AdminRank {
	// Just for me :)
	
	public OwnerRank(){
		super.setRankName("OWNER");
		super.setRankColor(ChatColor.DARK_RED);
	}
	
	@Override
	public void addCommands() {
		super.addCommands();
		ArrayList<String> commands = super.getCommands();
		commands.add("/stop");
	}
}