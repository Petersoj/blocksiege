package com.blocksiege.player.ranks;

import org.bukkit.ChatColor;

public class KnightRank extends DefaultRank {
	// This is a rank that can only be bought on the store
	
	public KnightRank(){
		super.setRankName("KNIGHT");
		super.setRankColor(ChatColor.BLACK);
	}
}