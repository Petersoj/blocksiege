package com.blocksiege.player;

import org.bukkit.entity.Player;

import com.blocksiege.BlockSiege;

public class GamePlayer {
	
	private BlockSiege plugin;
	
	private Player player;
	private Stats stats;
	
	public GamePlayer(BlockSiege plugin, Player player){
		this.plugin = plugin;
		this.player = player;
		
		this.stats = new Stats();
	}
	
	public BlockSiege getPlugin() {
		return plugin;
	}

	public Player getPlayer() {
		return player;
	}

	public Stats getStats(){
		return stats;
	}

}
