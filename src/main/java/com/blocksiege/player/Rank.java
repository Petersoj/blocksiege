package com.blocksiege.player;

import java.util.ArrayList;

import org.bukkit.ChatColor;

public abstract class Rank {
	// The super classes of this class will be singletons because there isn't much dynamic content for them.
	
	private String rankName;
	private ChatColor rankColor;
	private final ArrayList<String> commands;
	
	public Rank(){
		this.commands = new ArrayList<String>();
		
		this.addCommands();
	}
	
	public abstract void addCommands();

	public String getRankName() {
		return rankName;
	}

	public void setRankName(String rankName) {
		this.rankName = rankName;
	}

	public ChatColor getRankColor() {
		return rankColor;
	}

	public void setRankColor(ChatColor rankColor) {
		this.rankColor = rankColor;
	}

	public ArrayList<String> getCommands() {
		return commands;
	}
}
