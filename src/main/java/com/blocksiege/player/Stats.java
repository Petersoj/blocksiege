package com.blocksiege.player;

public class Stats {
	
	private int currentCoins, currentXP;
	private int currentKills, currentDeaths;
	private int currentWins, currentLosses, currentFlagsCaptured, currentTimePlayed;
	
	private int coins, xp;
	private float level;
	private int kills, deaths;
	private int wins, losses, flagsCaptured, timePlayed;
	
	// Capture a flag = +7 XP  and +3 coins
	
	// (Number of Damaged hearts / 2) = number of xp givin. 
	// If they got the kill, +1 coin and they get 1 extra XP point.
	
	// +50 xp and +50 coins for winning, +10 xp +10 coins for losing. ONLY IF THEY "PARTICIPATED"
	
	public float getLevel(boolean updateLevel){
		if(updateLevel){
			this.level = (float) Math.sqrt((double) (xp / 20) + 1); // Math.sqrt((xp / 10) / 2 + 1 ) http://www.desmos.com
		}
		return level;
	}
	
	// Currents
	
	public int getCurrentCoins() {
		return currentCoins;
	}

	public void incrementCurrentCoins(int currentCoins) {
		this.currentCoins += currentCoins;
	}

	public int getCurrentXP() {
		return currentXP;
	}

	public void incrementCurrentXP(int currentXP) {
		this.currentXP += currentXP;
	}

	public int getCurrentKills() {
		return currentKills;
	}

	public void incrementCurrentKills(int currentKills) {
		this.currentKills += currentKills;
	}

	public int getCurrentDeaths() {
		return currentDeaths;
	}

	public void incrementCurrentDeaths(int currentDeaths) {
		this.currentDeaths += currentDeaths;
	}

	public int getCurrentWins() {
		return currentWins;
	}

	public void incrementCurrentWins(int currentWins) {
		this.currentWins += currentWins;
	}

	public int getCurrentLosses() {
		return currentLosses;
	}

	public void incrementCurrentLosses(int currentLosses) {
		this.currentLosses += currentLosses;
	}

	public int getCurrentFlagsCaptured() {
		return currentFlagsCaptured;
	}

	public void incrementCurrentFlagsCaptured(int currentFlagsCaptured) {
		this.currentFlagsCaptured += currentFlagsCaptured;
	}

	public int getCurrentTimePlayed() {
		return currentTimePlayed;
	}

	public void incrementCurrentTimePlayed(int currentTimePlayed) {
		this.currentTimePlayed += currentTimePlayed;
	}

	
	// Totals

	public int getCoins() {
		return coins;
	}
	
	public void incrementCoins(int coins) {
		this.coins += coins;
	}
	
	public int getXp() {
		return xp;
	}
	
	public void incrementXp(int xp) {
		this.xp += xp;
	}
	
	public int getKills() {
		return kills;
	}
	
	public void incrementKills(int kills) {
		this.kills += kills;
	}
	
	public int getDeaths() {
		return deaths;
	}
	
	public void incrementDeaths(int deaths) {
		this.deaths += deaths;
	}
	
	public int getWins() {
		return wins;
	}
	
	public void incrementWins(int wins) {
		this.wins += wins;
	}
	
	public int getLosses() {
		return losses;
	}
	
	public void incrementLosses(int losses) {
		this.losses += losses;
	}
	
	public int getFlagsCaptured() {
		return flagsCaptured;
	}
	
	public void incrementFlagsCaptured(int flagsCaptured) {
		this.flagsCaptured += flagsCaptured;
	}
	
	public int getTimePlayed() {
		return timePlayed;
	}
	
	public void incrementTimePlayed(int timePlayed) {
		this.timePlayed += timePlayed;
	}
}
